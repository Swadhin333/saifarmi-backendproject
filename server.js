const express = require(`express`);
const { PORT } = require(`./src/config/config`);
const apiConnect = require(`./src/routes`);
const morgan = require("morgan");
const cors = require("cors");
const mongoose = require(`./src/config/database`);
const bodyParser = express.json();

function main() {
  const app = express();
  app.use(cors());
  app.use(bodyParser);
  mongoose;
  app.use(morgan("dev"));

  app.use(`/api`, apiConnect);
  app.listen(PORT, () => {
    console.log(`Listening on port: ${PORT}`);
  });
}
main();
