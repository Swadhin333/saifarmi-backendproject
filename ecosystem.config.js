module.exports = {
    apps: [
      {
        name: "nodetest_pro",
        script: "./server.js",
      },
    ],
    deploy: {
      production: {
        user: "ubuntu",
        host: "65.0.88.195",
        ref: "origin/master",
        repo: "git@gitlab.com:Swadhin333/node_backend_project.git",
        path: "/var/www/nodetest_pro",
        "post-deploy": "npm ci && npm run-script restart",
      }
    },
  };
  // module.exports = {
  //   apps: [
  //     {
  //       name: "bluddy-api",
  //       script: "./server.js"
  //     }
  //   ],
  //   deploy: {
  //     production: {
  //       user: "ubuntu",
  //       host: "13.234.212.227",
  //       key: "~/.ssh/bluddyApp.pem",
  //       ref: "origin/master",
  //       repo: "git@gitlab.com:therubbishnetwork/bluddy-backend.git",
  //       path: "/var/www/bluddy-api",
  //       "post-deploy": "npm install && npm run-script restart"
  //     }
  //   }
  // };