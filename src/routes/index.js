const apiRoutes = require(`express`).Router();

const userAuthRoute = require(`./userAuth`);

apiRoutes.use(`/userAuth`, userAuthRoute);

module.exports = apiRoutes;
