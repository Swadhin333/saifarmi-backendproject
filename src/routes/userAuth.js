const express = require(`express`);
const userAuthRoute = express.Router();
const bcrypt = require(`bcrypt`);
const saltRounds = 10;
const {
  employeeModel,
  salePointModel,
  credentialModel,
  higherauthModel,
} = require(`../models`);
const randomId = require(`../utils/randomId`);

//------super admin register-authontications!!
//-------------------------------employee----------------------------
//CURD--START
userAuthRoute.post(`/registeremployee`, (req, res) => {
  const employee = "empy";
  const employeeId = (
    employee.trim().substring(0, 4) + Math.floor(1000 + Math.random() * 9000)
  ).toUpperCase();
  credentialModel
    .findOne({
      $or: [{ emailId: req.body.emailId }, { mobileNo: req.body.mobileNo }],
    })
    .then((user) => {
      if (user) {
        res.json({
          stage: `unsuccess`,
          message: `input details is already registered!!`,
        });
      } else {
        if (
          req.body.userRole === "sales-executive" ||
          req.body.userRole === "sourcing" ||
          req.body.userRole === "stock-in-charge"
        ) {
          new credentialModel({
            employeeId,
            emailId: req.body.emailId,
            mobileNo: req.body.mobileNo,
          })
            .save()
            .then((user) => {
              new employeeModel({
                employeeId,
                name: req.body.name,
                "email.emailId": req.body.emailId,
                mobileNo: req.body.mobileNo,
                userRole: req.body.userRole,
                "address.line1": req.body.line1,
                "address.area": req.body.area,
                "address.state": req.body.state,
                "address.pincode": req.body.pincode,
              })
                .save()
                .then((user) => {
                  res.status(200).json({
                    stage: `success`,
                    message: `user ${req.body.name} is successfully registered as a ${req.body.userRole}!!`,
                  });
                })
                .catch((err) => {
                  res.status(500).json({
                    stage: `unsuccess`,
                    error: err,
                  });
                });
            })
            .catch((err) => {
              res.status(500).json({ stage: `unsuccess`, error: err });
            });
        } else {
          res.send("un authorized userrole");
        }
      }
    })
    .catch((err) => {
      res.status(500).json({ message: `unsuccess`, error: err });
    });
});
//read
userAuthRoute.post(`/fatchuserprofiles`, (req, res) => {
  var pipeline = [];
  if (req.body.userRole === "sourcing") {
    pipeline = [
      {
        $match: { userRole: req.body.userRole },
      },
      {
        $project: { salePointId: 0, assignClientId: 0 },
      },
    ];
  } else if (req.body.userRole === "sales-executive") {
    pipeline = [
      {
        $match: { userRole: req.body.userRole },
      },
      {
        $project: { assignVendorId: 0 },
      },
    ];
  } else {
    pipeline = [
      {
        $match: { userRole: req.body.userRole },
      },
      {
        $project: { assignVendorId: 0, assignClientId: 0, salePointId: 0 },
      },
    ];
  }
  employeeModel
    .aggregate(pipeline)
    .then((user) => {
      res.status(200).json({
        stage: `success`,
        message: `fatching user details!!`,
        data: user,
      });
    })
    .catch((err) => {
      res.status(500).json({
        stage: `unsuccess`,
        message: `internal server error!!`,
        error: err,
      });
    });
});
//update
userAuthRoute.post(`/edituserdetails/:employeeId`, (req, res) => {
  credentialModel
    .findOne({ employeeId: req.params.employeeId })
    .then((credUser) => {
      credentialModel
        .updateOne(
          { _id: credUser._id },
          {
            $set: {
              emailId: req.body.emailId,
              mobileNo: req.body.mobileNo,
            },
          }
        )
        .then((isDone) => {
          employeeModel
            .findOne({ employeeId: req.params.employeeId })
            .then((empData) => {
              employeeModel
                .updateOne(
                  { _id: empData._id },
                  {
                    $set: {
                      "email.emailId": req.body.emailId,
                      mobileNo: req.body.mobileNo,
                    },
                  }
                )
                .then((user) => {
                  res.status(200).json({
                    stage: `success`,
                    message: `user successfully updated!!`,
                  });
                })
                .catch((err) => {
                  res.json({
                    stage: `unsuccess`,
                    message: `emplooye updation failed`,
                    error: err,
                  });
                });
            })
            .catch((err) => {
              res.json({
                stage: `unsuccess`,
                message: `emplooye dose not exist`,
                error: err,
              });
            });
        })
        .catch((err) => {
          res.json({
            stage: `unsuccess`,
            message: `cradential updation failed`,
            error: err,
          });
        });
    })
    .catch((err) => {
      res.json({
        stage: `unsuccess`,
        message: `credential dose not exist!!`,
        error: err,
      });
    });
});
//delete
userAuthRoute.delete(`/removeingemployee/:employeeId`, (req, res) => {
  credentialModel
    .findOne({ employeeId: req.params.employeeId })
    .then((credUser) => {
      credentialModel
        .deleteOne({ _id: credUser._id })
        .then((isDone) => {
          employeeModel
            .findOne({ employeeId: req.params.employeeId })
            .then((empData) => {
              employeeModel
                .deleteOne({ _id: empData._id })
                .then((user) => {
                  res.status(200).json({
                    stage: `success`,
                    message: `user deleted successfully!!`,
                  });
                })
                .catch((err) => {
                  res.status(500).json({
                    stage: `unsuccess`,
                    message: `employee deletion failed!!`,
                    error: err,
                  });
                });
            })
            .catch((err) => {
              res.status(500).json({
                stage: `unsuccess`,
                message: `employee not found!!`,
                error: err,
              });
            });
        })
        .catch((err) => {
          res.status(500).json({
            stage: `unsuccess`,
            message: `credential deletion failed!!`,
            error: err,
          });
        });
    })
    .catch((err) => {
      res.status(500).json({
        stage: `unsuccess`,
        message: `credential not found!!`,
        error: err,
      });
    });
});
//CURD--END
//---------------------------employee(End)----------------------------

//---------------------------sale-point(START)----------------------------
//CURD--START
userAuthRoute.post(`/salepoint_registration`, (req, res) => {
  const salepoint = "spid";
  const salePointId = (
    salepoint.trim().substring(0, 4) + Math.floor(1000 + Math.random() * 90000)
  ).toUpperCase();
  salePointModel
    .findOne({ salePointName: req.body.salePointName })
    .then((user) => {
      if (user) {
        res.json({
          stage: `unsuccess`,
          message: `this sale-point name is already exist!!`,
        });
      } else {
        new salePointModel({
          salePointId,
          salePointName: req.body.salePointName,
          "address.line1": req.body.line1,
          "address.area": req.body.area,
          salePointIncharge: req.body.salePointIncharge,
          "address.district": req.body.district,
          "address.state": req.body.state,
          "address.pincode": req.body.pincode,
        })
          .save()
          .then((user) => {
            res.status(200).json({
              stage: `success`,
              message: `sale-point ${req.body.salePointName} is successfully registered in ${req.body.state}-${req.body.line1}-${req.body.area}!!`,
            });
          })
          .catch((err) => {
            res.status(500).json({ stage: `unsuccess`, error: err });
          });
      }
    })
    .catch((err) => {
      res.status(500).json({ message: `unsuccess`, error: err });
    });
});
//read
userAuthRoute.get(`/getallsalepoint`, (req, res) => {
  salePointModel
    .find({})
    .then((resp) => {
      res
        .status(200)
        .json({ stage: `success`, message: `fatching data!!`, data: resp });
    })
    .catch((err) => {
      res.status(500).json({
        stage: `unsuccess`,
        message: `internal server error!!`,
        error: err,
      });
    });
});
userAuthRoute.get(`/getsalepoint/:salePointId`, (req, res) => {
  salePointModel
    .findOne({ salePointId: req.params.salePointId })
    .then((resp) => {
      res
        .status(200)
        .json({ stage: `success`, message: `fatching data!!`, data: resp });
    })
    .catch((err) => {
      res.status(500).json({
        stage: `unsuccess`,
        message: `internal server error!!`,
        error: err,
      });
    });
});
//update
//delete
// userAuthRoute.get(`/removesalepoint/:salePointId`, (req, res) => {
//     salePointModel
//       .findOne({salePointId : req.params.salePointId})
//       .then((resp) => {
//         res
//           .status(200)
//           .json({ stage: `success`, message: `fatching data!!`, data: resp });
//       })
//       .catch((err) => {
//         res
//           .status(500)
//           .json({
//             stage: `unsuccess`,
//             message: `internal server error!!`,
//             error: err,
//           });
//       });
//   });

//CURD--END
//---------------------------sale-point(END)----------------------------
//------super admin register-authontications!!

//-----------CEO registation-authantication!!
//CURD--START
userAuthRoute.post(`/authregistrationbyceo`, (req, res) => {
  const employee = "empy";
  const employeeId = (
    employee.trim().substring(0, 4) + Math.floor(1000 + Math.random() * 9000)
  ).toUpperCase();
  higherauthModel
    .findOne({ userRole: req.body.userRole })
    .then((user) => {
      if (user) {
        res.json({ status: `unsuccess`, message: `permission denite!!` });
      } else {
        credentialModel
          .findOne({
            $or: [
              { emailId: req.body.emailId },
              { mobileNo: req.body.mobileNo },
            ],
          })
          .then((user) => {
            if (user) {
              res.json({
                state: `unsuccess`,
                message: `invalid emailId or mobile number!!`,
              });
            } else {
              if (
                req.body.userRole === "super-admin" ||
                req.body.userRole === "accountant"
              ) {
                new credentialModel({
                  employeeId,
                  emailId: req.body.emailId,
                  mobileNo: req.body.mobileNo,
                })
                  .save()
                  .then((user) => {
                    const password = randomId("", 8, true);
                    console.log(password);
                    bcrypt.hash(password, saltRounds, function (err, hash) {
                      if (err) {
                        res.json({ error: err });
                      } else {
                        new higherauthModel({
                          employeeId,
                          name: req.body.name,
                          "email.emailId": req.body.emailId,
                          userRole: req.body.userRole,
                          mobileNo: req.body.mobileNo,
                          password: hash,
                          "address.line1": req.body.line1,
                          "address.area": req.body.area,
                          "address.state": req.body.state,
                          "address.pincode": req.body.pincode,
                        })
                          .save()
                          .then((user) => {
                            res.status(200).json({
                              stage: `success`,
                              message: `${req.body.name} is successfully registered as a ${req.body.userRole}!!`,
                              password: `password is ${password}`,
                            });
                          })
                          .catch((err) => {
                            res
                              .status(500)
                              .json({ stage: `unsuccess`, error: err });
                          });
                      }
                    });
                  })
                  .catch((err) => {
                    res.status(500).json({ stage: `unsuccess`, error: err });
                  });
              } else {
                res.json({
                  stage: `unsuccess`,
                  message: `unauthorized user-role!!`,
                });
              }
            }
          })
          .catch((err) => {
            res.status(500).json({ stage: `unsuccess`, error: err });
          });
      }
    })
    .catch((err) => {
      res.status(500).json({ stage: `unsuccess`, error: err });
    });
});
//read
//update
//----email & mobile no. update-----
//----reset/password update---------
//delete
//CURD--END
//-----------CEO(END) registation-authantication!!

//------------------------------------------------CEO REG-------------------------------------------------------

//------------------------------------------------LOG IN-----------------------------------------------------------
//--Mobile app login & Profile
userAuthRoute.post(`/employeelogin`, (req, res) => {
  employeeModel
    .findOne({ mobileNo: req.body.mobileNo })
    .then((resp) => {
      if (
        resp.userRole === "sale-executive" ||
        resp.userRole === "stock-in-charge" ||
        resp.userRole === "sourcing" ||
        resp.userRole === "sale-point-admin"
      ) {
        res
          .status(200)
          .json({
            stage: `success`,
            message: `${resp.userRole} Loged in successfully!!`,
          });
      } else {
        res.json({ stage: `invalid userRole!!!` });
      }
    })
    .catch((err) => {
      res.status(500).json({
        stage: `unsuccess`,
        message: `user dose not exist`,
        error: err,
      });
    });
});
//--superadmin & accauntant Login
//--CEO log in
module.exports = userAuthRoute;
