const employeeModel = require(`./userDetails/employeeModel`);
const salePointModel = require(`./userDetails/salePointModel`);
const higherauthModel = require(`./userDetails/higherauthModel`);
const credentialModel = require(`./userDetails/credentialModel`);
const vendorDetailsModel = require(`./userDetails/vendorDetailsModel`)
module.exports = {
  employeeModel,
  salePointModel,
  higherauthModel,
  credentialModel,
  vendorDetailsModel
};
