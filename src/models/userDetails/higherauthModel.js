const mongoose = require(`mongoose`);

const address = mongoose.Schema({
  line1: { type: String, required: true },
  area: { type: String, required: true },
  state: { type: String, required: true },
  pincode: { type: String, required: true },
});

const email = mongoose.Schema({
  emailId: { type: String, required: true, unique: true },
  isVerified: { type: Boolean, required: true, default: false },
});
const higherauthModel = new mongoose.Schema(
  {
    employeeId: { type: String, required: true },
    name: { type: String, required: true, uppercase : true },
    email: email,
    userRole: { type: String, required: true },
    mobileNo: {
      type: String,
      required: true,
      unique: true,
      minlength: 10,
      maxlength: 10,
    },
    password: { type: String, required: true, uppercase : true },
    address: address,
    activeDevice: { type: String, required: false, default: null },
  },
  { timestamps: true }
);

module.exports = mongoose.model(`tbl_higherauthModel`, higherauthModel);
