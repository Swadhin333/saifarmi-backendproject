const mongoose = require(`mongoose`);

const address = mongoose.Schema({
  line1: { type: String, required: true, trim: true },
  area: { type: String, required: true, trim: true },
  district: { type: String, required: true, trim: true },
  pincode: { type: String, required: true, trim: true },
  state: { type: String, required: true, trim: true },
  country: { type: String, required: true, trim: true, default: "India" },
});

const salePointModel = new mongoose.Schema(
  {
    salePointId: { type: String, required: true },
    salePointName: { type: String, required: true, unique: true },
    salePointIncharge: {
      salePointAdminName: {
        type: String,
        required: false,
        default: "NOT ASSIGED",
      },
      employeeId: { type: String, required: false, default: "NOT ASSIGED" },
    },
    address: address,
  },
  { timestamps: true }
);

module.exports = mongoose.model(`tbl_salePointModel`, salePointModel);
