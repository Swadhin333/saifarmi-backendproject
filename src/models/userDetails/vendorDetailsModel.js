const mongoose = require(`mongoose`);

const address = mongoose.Schema({
  line1: { type: String, required: true, trim: true },
  area: { type: String, required: true, trim: true },
  pincode: { type: String, required: true, trim: true },
});

const bankDetails = mongoose.Schema({
  bankName: { type: String, required: true, trim: true, uppercase: true },
  acHolderName: { type: String, required: true, trim: true, uppercase: true },
  acNumber: { type: String, required: true, trim: true, uppercase: true },
  ifscCode: { type: String, required: true, trim: true, uppercase: true },
});
const employeeDetails = mongoose.Schema({
  employeeId: {type: String, required: true},
  employeeName: {type: String, required: true},
});

const vendorDetailsModel = new mongoose.Schema(
  {
    employeeDetails: employeeDetails,
    vendorId: {type: String, required: true, uppercase : true},
    vendorName: {type: String, required: true},
    emailId: {type: String, required: false},
    mobileNo: {type: String, required: true, unique : true},
    address: address,
    bankDetails: bankDetails,
  },
  { timestamps: true }
);

module.exports = mongoose.model(`tbl_vendorDetailsModel`, vendorDetailsModel);
