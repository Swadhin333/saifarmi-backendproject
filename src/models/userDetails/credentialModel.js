const mongoose = require(`mongoose`);

const credentialModel = new mongoose.Schema({
  employeeId: { type: String, required: true, unique: true },
  emailId: { type: String, required: true , unique: true},
  mobileNo: {
    type: String,
    required: true,
    unique: true,
    minlength: 10,
    maxlength: 10,
  },
});

module.exports = mongoose.model(`tbl_credential`, credentialModel);
