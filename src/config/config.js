const dotenv = require(`dotenv`)

dotenv.config();
module.exports = {
    PORT: process.env.PORT,
    MONGO_LINK : process.env.MONGO_LINK,
    SECRET_KEY : process.env.SECRET_KEY
}