const mongoose = require(`mongoose`)
const {MONGO_LINK} = require(`./config`);

mongoose.connect(MONGO_LINK, {useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false, useCreateIndex: true})
        .then(()=>{console.log(`database connected successfully`)})
        .catch((e) => {console.log(`connection failed`, e)})

module.exports = mongoose;