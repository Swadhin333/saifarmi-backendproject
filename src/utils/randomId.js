module.exports = function makeid(startWith, length, alpha) {
  var text = startWith ? startWith : "";
  var possible = `0123456789${alpha ? "ABCDEFGHIJKLMNOPQRSTUVWXYZ" : ""}`;

  // Generating random id with specified length
  for (var i = 0; i < length; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
};
